<%@ page import = "mainPackage.AddMethods" %>
<%@ page import = "java.util.List" %>
<!DOCTYPE html>
<html lang = "en">
    <head>
        <meta charset = "UTF-8">
        <title>Items list</title>
    </head>
    <body>
    <form action = "/add" method = "post">
        <input type = "text" name = "item" placeholder = "add a item"/>
        <input type = "submit" name = "submit" value = "add"/>
    </form>
        <ul>
            <%
                List<String> items = AddMethods.writeWhole();
                for (int i = 0; i < items.size(); i++) {
                    String url = "/delete?line=" + i;
                    String item = items.get(i);
                    out.write("<li>" + item + " <a href =" + url + ">x</a></li>");
                }
            %>
        </ul>
    </body>
</html>
