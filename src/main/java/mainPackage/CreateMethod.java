package mainPackage;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static mainPackage.AddMethods.write;

public class CreateMethod {
    // deletes old and creates new file
    public static void newFile(List<String> lines) {
        File newFile = new File(AddMethods.LI_DIR);
        if (newFile.delete()) {
            try {
                if (newFile.createNewFile()) {
                    for (String line : lines) {
                        write(line);
                    }
                } else {
                    System.out.println("Can't create file");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Can't delete file");
        }
    }

}
